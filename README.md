# Optimización de despliegues con Terraform como Pipelines de Gitlab CI.

![IMGTitulo](./images/terraform_gitlab.png)

## Índice
1. [Introducción](#1-introducci%C3%B3n)
2. [Surge una necesidad](#2-surge-una-necesidad)
3. [Prueba de Concepto](#3-prueba-de-concepto)
4. [Objetivos](#4-objetivos)
5. [Pre-Requisitos](#5-pre-requisitos)
6. [Diseño de Solución](#6-dise%C3%B1o-de-soluci%C3%B3n)
    - 6.1 [Arquitectura](#61-arquitectura)
    - 6.2 [Componentes](#62-componentes)
    - 6.3 [Pipelines](#63-pipelines)
7. [Conceptos y Mejoras]()
    - 7.1 [GitLab API](#71-gitlab-api)
    - 7.2 [Terraform Remote Back-end for StateFiles](#72-terraform-remote-back-end-for-statefiles)
    - 7.3 [Módulos de Terraform como Gitlab Projects](#73-m%C3%B3dulos-de-terraform-como-gitlab-projects)
    - 7.4 [Pipeline Multi Proyectos y Pipelines hijos (Child)](#74-pipeline-multi-proyectos-y-pipelines-hijos-child)
    - 7.5 [Funcionamiento y Beneficios](#75-funcionamiento-y-beneficios)
8. [Hashicorp Vault como fuente de secretos (pendiente de implementación)](#8-hashicorp-vault-como-fuente-de-secretos-pendiente-de-implementaci%C3%B3n)
9. [Implementación](#9-implementaci%C3%B3n)
10. [Conclusiones](#10-conclusiones)

## 1. Introducción

Terraform es una herramienta fantástica para administrar infraestructura en la nube, especialmente si sus servicios están alojados en varios *"cloud providers"*. Más allá de un código eficiente y performante, esta herramienta suele ser un problema si no se utiliza de forma de correcta desde la optimización y organización de sus recursos.

En este artículo, demostraremos cómo construir un proceso completo de CI/CD con pipelines de despliegue de Terraform usando GitLab CI.

## 2. Surge una necesidad
Dependiendo de la escala de su organización, es habitual que su Cloud Engineer comience ejecutando comandos de Terraform de forma local, dado que sencillo, rápido y eficiente...Si!, quizás el código de despliegue se encuentra en un repositorio Git pero muchas veces el backend de State Files se guarda en entorno de donde se ejecutó. Todo va bien hasta que te das cuenta que, al igual que el código de Terraform, también los archivos de estados deben ser resguardados de forma central, seguros y compartidos. Por ejemplo, un caso concreto de esta necesidad surge cuando queremos liberar y borrar los recursos Cloud y no tenemos el `*.tfstate` para ejecutar un "terraform destroy". Aqui surge nuestra primer problema, necesitamos un backend de Terraform centralizado, velóz y seguro.

Hoy en día la reutilización de códido de despĺiegue es habitual en los Pipelines de CI/CD, es por ellos que empezamos a "modulalizar" los despliegues, creamos y subimos fragmentos de código de una funcionalidad especifica a repositorios externos pero... ¿qué sucede si queremos que nuestros proyectos de GitLab CI lleven seguimientos de los cambios, issues, branches del nuestra infraestructura como código o incluso frente a la necesidad de manejar de forma centralizada el `input` y `output` Terraform como artefactos de nuestro pipeline CI? 

Por otro lado, ¿qué sucede con los secretos que utiliza Terraform para administrar su infraestructura? Son variables de entorno facilmente expuestas? Y quizás también se suelen administrar de forma local para las pruebas ¿Pero es segura, eficiente y ordenada esta práctica? Por ejemplo, podría suceder que el departamento de Seguridad Infomática no desea, ni tiene permido, dibulgar y entregar secretos como ser Access y Secrets Keys de AWS. ¿Cómo solucionaría esto, es decir, como usaría Terraform para se conecte a AWS y comience a crear recursos si el Cloud Engineer o el Developer no conoce las credenciales?

Por último, todos sabemos que los desarrolladores están acostumbrados a utlizar pipelines CI para el desarrollo de código y para las pruebas de integración de aplicaciones, pero... ¿qué pasa con Terraform? ¿Se pueden aplicar técnicas de integración y entrega continua a su código Terraform? 

## 3. Prueba de Concepto

No hay mejor forma que si surge una necesidad, probar que existe una solución. Para ello en el corriente repositorio Ud. encontrara todo el código e información necesaria para probar una exitosa solución a la necesidad anteriomente planteada.

Se utilizará como ejemplo una aplicación sencilla basada en dos microservicios: un Back-End API Rest desarrollada con Stringboot (Java), un Front-End web en React y persistencia de datos en MongoDB.
La infraestructura será desplegada con Terraform a demanda una vez compilada y "contenerizada" los microservicios. 

## 4. Objetivos

* Utilización de Pipelines de Gitlab CI para el manejo de infraestructura con Terraform.

* Resguardo seguro y eficiente de los State files de Terraform.
* Manejar modulos de Terraform como proyectos de GitLab.
* Gestión completa de variables de entrada y salida de Terraform como artefactos de Pipelines CI.
* Probar la integracion de Gitlab CI Pipelines en multi proyectos funcionales y resilentes a fallos de Jobs de despliegue.
* Gestión y utilización de secretos reduciendo la exposición y aumentando la seguridad.

## 5. Pre-Requisitos

* Cuenta Premium/Silver en Gitlab para la descarga de artefactos en Pipelines multi proyectos. Si desea usar una cuenta Gitlab free, los artefactos deben ser publicados en *File Sharing Servers* externos como AWS S3.
* Cuenta en AWS y sus credenciales, este será IaaS de despliegue.
> **IMPORTANTE:** Las credenciales de AWS deben tener permisos para crear Roles y Póliticas en IAM.

## 6. Diseño de Solución
### 6.1 Arquitectura

**Aplicativa**: Una arquitectura basada en micro-servicios Full-Stack con persistencia de datos en una base MongoDB. Mayor información [aquí](./app/README.md). 

**CI/CD**:

* **Gitlab CI** para pipelines, Respositorio Git, Repositorio de Artefactos, Repositorio de Terraform StateFiles. 
* **Docker Hub** como repositorio de imagenes de la aplicación.


**Infraestructura**:
* **Kubernetes** como plataforma como servicio.
* **Ingress Controller** para la exposicion y balanceo de carga de los servicios del cluster de Kubernetes.
* **Prometheus** para logging y monitoreo de métricas y Alert Manager.
* **Grafana** como dashboard de visualización de Prometheus y deshboad de Kubernetes.
### 6.2 Componentes
* **AWS**: Infraestructura como Servicio.
![IMGPods](./images/ec2-minikube.png)

* **Intancia AWS EC2 t3a-medium**: Minikube - Single Node Kubernetes Solution.
* **Volumenes Persistentes**: Aprovisionados automaticamente de volumenes AWS GP2
* **Kubernetes Ingress Control Pod**: Nginx Proxi reverso y balanceador de carga.


![IMGPods](./images/pods.png)
### 6.3 Pipelines
Los pipelines involucrados en la solución son 4:


**Parent/Main Pipeline o Pipeline Pricipal:**
- #1 [.gitlab-ci.yml](./.gitlab-ci.yml) Pipeline encargado del CI, de las pruebas, flujo y control. Este pipeline lanza los otros tres pipelines. 

**Aprovisionamiento de Infra:**
- #2 [.gitlab-ci.yml](https://gitlab.com/flgonzalez/aws-vpc/-/blob/master/.gitlab-ci.yml) Pipeline para el creación y aprovisionamiento de recursos de red en AWS .
- #3  [.gitlab-ci.yml](https://gitlab.com/flgonzalez/aws-minikube/-/blob/master/.gitlab-ci.yml) Pipeline para el creación y aprovisionamiento de recursos de cómputo en AWS y despliegue de Kubernetes.

**Deploy CI - Despliegue de la Aplicación en K8s y sus servicios:**
- #4 [deploy-ci.yml](./deploy-ci.yml) Pipeline Hijo con el depliegue del CI.

## 7. Conceptos y Mejoras
### 7.1 GitLab API

Las ventajas y funcionalidades que nos trae la API de GitLab son muchas pero hoy nos toca hablar de aquellas que efectivamente nos ayuda a gestionar nuestra IaC con Pipelines de Terraform en GitLab. A continuación se menciona algunos casos de uso más relevantes que se pueden lograr con Terraform y la API de Gitlab:

* Gestión e integración de infraestructura con sistemas externos, por ejemplo aprovisionamiento self-service y auto-remedation.
* Seguimiento y gestión de costos asociados a los recursos activos de infraestructura.
* Gestion de secretos de "Cloud Providers" con sistemas externos, por ejemplo Hashicorp Vault.
* GitOps workflows/actions.
* Monitoreo y plan de cumplimiento (compliance).
* Entre otros....

Particularmente en esta prueba de concepto, la API de Gitlab es un componente fundamental para lanzar, reanudar o personalizar Pipelines CD en un esquema multi proyecto. Por otro lado tambien es un componente fundamental para compartir artefactos de manera segura entre proyectos. Estas dos funciones son explicadas en detalles en [Funcionamiento y Beneficios](./docs/README.md).

Ejemplo de utilización de la API para lanzar un pipeline:
```bash
curl -X POST -F token=${CI_PIPELINE_TOKEN} -F "ref=master" -F "variables[key1]=value" -F "variables[key2]=value" -F "variables "https://gitlab.com/api/v4/projects/23008422/trigger/pipeline"

```
### 7.2 Terraform Remote Back-end for StateFiles

Los archivos de estado de Terraform se usa para conciliar los recursos implementados con configuraciones de Terraform. El estado permite que Terraform sepa qué recursos de infraestructura debe agregar, actualizar o eliminar. De forma predeterminada, el estado de Terraform se almacena localmente cuando se ejecuta el comando "terraform apply". Esta configuración no es ideal por los siguientes motivos:

* El estado local no funciona bien en un equipo ni en un entorno de colaboración.
* El estado de Terraform puede incluir información confidencial.
* Almacenar el estado localmente aumenta la posibilidad de eliminación accidental.

Terraform admite la persistencia del estado en el almacenamiento remoto. Uno de estos back-end compatible es HTTP y GitLab provee una APi y un repositorio para estos archivos. A continuación se muestra un ejemplo cómo configurar y GitLab con este fin.

En el archivo de Terraform: [main.tf](./main.tf)
```tf
terraform {
  required_version = "~> 0.13.2"
  backend "http" {}
}
```
En el manifiesto del Pipeline: [deploy-ci.yml](./deploy-ci.yml) o en Bash ;)
```bash
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/23008422/terraform/state/aws-minikube" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/23008422/terraform/state/aws-minikubes/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/23008422/terraform/state/aws-minikube/lock" \
    -backend-config="username=flgonzalez" \
    -backend-config="password=${GITLAB_API_KEY}" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```
> **Nota**: En la PoC se utiliza imagen docker de Terraform provista por GitLab y la configuración del "remote back-end" se hace de forma automatica. Es por ello que podrá observar [deploy-ci.yml](./deploy-ci.yml) que el comando de terraform es **gitlab-terraform**.  Mayor información en: https://docs.gitlab.com/ee/user/infrastructure/

Información oficial de Terraform para Back-end remote: https://www.terraform.io/docs/state/remote.html

### 7.3 Módulos de Terraform como Gitlab Projects

Con la utilización de los proyectos de Gitlab para modularizar su código de Terraform tiene los siguientes beneficios (entre muchos otros):

* Repositorios de IaC, Terraform State files e imagenes de Terraform.
* Issues y merge requests: Issue tracker, Time tracking, etc.
* GitLab CI/CD: Deployment Pipelines, Manejo de runners, Logs de ejecución de los diferentes Jobs (init, validate, plan, apply, destroy), etc..
* GitLab: Utilización de una API para la gestión de pipelines de despliegue de Terraform.
* Otros: https://docs.gitlab.com/ee/user/project/#project-features

La ejecución de la PoC utiliza dos proyectos de GitLab adicionales donde se encuentra el código de despliegue de Terraform de forma modular. 

* Proyecto AWS VPC - Id 23007022: En este proyecto se encuentra las definiciones y manifiestos para crear en AWS una VPC y Subnets con Terraform. Mayor información en https://gitlab.com/flgonzalez/aws-vpc/-/blob/master/README.md

* Proyecto AWS Minikube - Id 23008422: En este proyecto se encuentra las definiciones y manifiestos para desplegar en AWS con Terraform una instancia de EC2 para luego la istalación de un Kubernetes simple de un nodo. Mayor información y código del módulo en
https://gitlab.com/flgonzalez/aws-minikube/-/blob/master/README.md

### 7.4 Pipeline Multi Proyectos y Pipelines hijos (Child)

Una correcta utilización de los patrones de desarrollo y despliegue de micro-servicios demanda una correcta segregación de funciones y responsabilidades en el proceso completo de CI/CD. Para ello, entre otras cosas, se requiere la implementación de Pipeline de CI por un lado y uno o varios Pipelines de despliegue por el otro lado.
Con GitLab CI y una la utilización de Pipelines Multi-Proyectos podemos manejar de forma modular y sencila la integración de diferentes componetes, etapas, responsabilidades y alcances de punta a punto de nuestro proceso CI/CD.

En nuestro ejemplo son 4 (cuatro) los Pipelines que se ejecutarán de punta a punta. Pero solo existe un Pipeline que comanda todo y cuál detalla a continuación:

* **Pipeline Padre/Principal o Parent:** Es el encargado del CI, es decir, de la compilación del código de la aplicación, armado de las imagenes docker y pruebas de integración de los micro-servicios. Luego este pipeline está encargado del flujo, control y ejecución de los pipelines de aprovisionamiento de la Infraestructura (los dos proyectos mencionados anteriormente). A continuación se ejecuta un pipeline hijo con el propio despliegue de los micro-servicios en Kubernetes y finalmente realiza las pruebas "End-to-End" de toda la solución.

Información oficial de arquitectura de pipelines:
* [Multi-project pipelines](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html) 
* [Child/Parent Pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html)

### 7.5 Funcionamiento y Beneficios

* Si un pipeline de CD falla no es necesario ejecutar nuevamente todo el pipeline de CI. Se puede arreglar el componente de CD, "pushear" una nueva versión del código de Terraform y re-lanzar el Stage de CI de pruebas.
* Fácil de revertir a versiones anteriores de infraestructura.
* Infraestructura inmutable garantizada.
* Se puede manejar secretos segregados por proyectos, es decir los secretos para desplegar infraestructura son parte de los proyectos de CD y no de los projectos de CI.
* Cada proyectos de GitLab tiene sus propios repositorios de artefactos y contenedores. Esto es muy útil para el uso y almacenamiento de variables de entrada y salida de Terraform como artefactos.
* Cada proyecto de Gitlab tiene sus propios colaboradores ("Members"), por lo tanto se puede hacer un ajuste fino sobre la visibilidad y uso de todos los componentes de CI/CD.
* Las pruebas de CI pueden separse de las pruebas de CD.
* Al utilizar "stages" manuales se puede decidir el momento adecuado de promocionar de entorno o mismo lanzar a producción.
* Cambios en la infraestructura disparan las pruebas del sistema adecuadas
* Fácil de implementar entornos de prueba y desarrollo separados para cada desarrollador.
* Optimización de costos de la nube al aprovisionar infraestructura inmediatamente después de CI o al automatizar con "terraform destroy" luego de fallas o rollbacks.

## 8. Hashicorp Vault como fuente de secretos (pendiente de implementación)

Los secretos representan información confidencial que necesitan los Jobs de CI, por ejemplo string de conexión a base de datos, o información que necesita los Jobs de CD para crear recursos en AWS, por ejemplo. Esta información confidencial puede ser tokens de API, credenciales de base de datos o password privadas. Integrando Hashicorp Vault a Gitlab los secretos son obtenidos y entregados al pipeline en tiempo de ejecución sin necesidad de almacenarlos en variables de entorno.

GitLab ha seleccionado Vault de HashiCorp como el primer proveedor admitido y KV-V2 y como el primer motor de secretos certificado.

GitLab se autentica utilizando el método de autenticación JSON Web Token (JWT) de Vault, utilizando el JSON Web Token del Job (CI_JOB_JWT) introducido en GitLab 12.10.

![IMGTitulo](./images/gitlab_vault_workflow.png)


Más información en: https://docs.gitlab.com/ee/ci/secrets/

## 9. Implementación

Por favor seguir atentamente las instrucciones para armar el escenario de pruebas, es muy sencillo dado que está todo automatizado: [Instrucciones.](./implementacion/README.md)

## 10. Conclusiones
La aplicación del concepto Gitflow para Terraform en Gitlab se traduce en un flujo de trabajo potente y altamente productivo usando Gitlab CI para la infraestructura como código e implementar una infraestructura de nube inmutable.




