# Student-App
![IMGTitulo](./k8s_serv.png)

Full stack example application on Kubernetes. It's about a Student CRUD application that uses React as Front end, Spring Boot as back end and MongoDB as persistance layer.

![IMGTitulo](./student-app.jpg)
