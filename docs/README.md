# (En desarrollo )

## Funcionamiento en detalle Pipelines Multi-Project
- https://zerotoprod.com/posts/gitlab-ci-advanced/

## Funcionamiento en detalle de Integración de Hashicorp Vault
![IMGTitulo](./vault_gitlab.png)

- https://mittaltarun9715.medium.com/using-hashicorp-vault-for-gitlab-ci-variables-5f1c921ff304
- https://gitlab.com/gitlab-org/gitlab/-/issues/9981
- https://docs.gitlab.com/ee/ci/secrets/
- https://about.gitlab.com/blog/2020/02/13/vault-integration-process/

## Terraform: Providers, Resources and Modules usados

- https://github.com/OpenQAI/terraform-helm-release-prometheus-operator
- https://registry.terraform.io/providers/hashicorp/helm/latest
- https://registry.terraform.io/providers/hashicorp/kubernetes/latest
- https://github.com/ferluko/terraform-aws-minikube
- https://gitlab.com/flgonzalez/aws-vpc
- https://github.com/invidian/terraform-provider-sshcommand
