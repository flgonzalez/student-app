# Armado del escenario de prueba y ejecución

1. **Importar (clonar)** los siguientes tres proyectos a su GitLab desde "New Project" menu. Dejar todos los parámetros por defecto.

    * https://gitlab.com/flgonzalez/student-app
    * https://gitlab.com/flgonzalez/aws-minikube
    * https://gitlab.com/flgonzalez/aws-vpc

 
2. Crear Git **API private key**

    * Global Settings > Access Tokens
        - Nombre/Name: *foo*
        - Set Expiration: *next year*
        - Scopes checked: *api, write_registry*
    
    > **IMPORTANTE**: Tomar nota en lugar seguro del token, el mismo no se podrá visualizar nuevamente.

3. Configuración del Main Pipeline del proyecto "student-app"

    * Editar [.gitlab-ci.yml](./.gitlab-ci.yml) cambiando `23007022` por el ID de su proyecto AWS_VPC importado en el punto 1. 
    * Editar [.gitlab-ci.yml](./.gitlab-ci.yml) cambiando `23008422` por el ID de su proyecto AWS_Minikube importado en el punto 1.
    * Commit & Push.

     > **IMPORTANTE**: Por defecto Gitlab busca si existe un archivo llamado `.gitlab-ci.yml`, si lo encuentra automaticamente ejecuta el pipeline por cada cambio (merge o push) del mismo branch.

4. Configuración de proyecto "aws-minikube"

    * Project Settings > CI /CD > Pipeline Triggers > *Ingresar descripción: por ejemplo Multi-Project Tigger* > **Add Trigger**.
    * Project Settings > CI /CD > **Variables**:
        - **AWS_SECRET_ACCESS_KEY** = [Su AWS secret access key]
        - **AWS_ACCESS_KEY_ID** = [Su AWS access key]
        - **GITLAB_API_KEY** = [token creado en el punto anterior] 
        - **TF_VAR_env_name** = [Pre-fix name de los recursos de AWS a crear] ej: `ferluko`

    > **NOTA**: Leer documentación del módulo de Terraform para la customización de otras variables, por ejemplo tamaño del volumen persistente del Minikube o tipo de instancia EC2, todas estas variables deben ser definidas en el pipeline comenzando con **TF_VAR_**: [Link](https://github.com/ferluko/terraform-aws-minikube)

5. Configuración de proyecto "aws-vpc"

    * Project Settings > CI /CD > Pipeline Triggers > Ingresar descripción: *por ejemplo Multi-Project Tigger* > Add Trigger.
    * Project Settings > CI /CD > Variables:
        - **AWS_SECRET_ACCESS_KEY** = [Su AWS secret access key]
        - **AWS_ACCESS_KEY_ID** = [Su AWS access key]
        - **GITLAB_API_KEY** = [token creado en el punto 2]
        - **TF_VAR_aws_region** = [región de AWS] ej: `us-east-2`
        - **TF_VAR_aws_zones** = [AZ de AWS, es un array!] ej: `["us-east-2a"]`
        - **TF_VAR_private_subnets** = [Subnet privadas?] ej: `false`
        - **TF_VAR_vpc_cidr** = [CIDR de la VPC] ej: `10.17.0.0/16`
        - **TF_VAR_vpc_name** = [Nombre de la VPC] ej: `ferluko`
        - **TF_VAR_vpc_name** = [Tag de la VPC/subnets] ej: `{App = "Minikube"}`

    > **NOTA**: Leer documentación del módulo de Terraform para la customización de las variables, por ejemplo otras redes o nombres, todas estas variables deben ser definidas en el pipeline comenzando con TF_VAR_: [Link](https://gitlab.com/flgonzalez/aws-vpc)

6. Proyecto "student-app"

    * CI/CD > **Run Pipeline** and enjoy!


