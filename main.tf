terraform {
  backend "http" {}
  required_providers {
    helm = {
      source = "hashicorp/helm"
      version = "1.3.2"
    }
  }
}

provider "kubernetes" {
	config_path = "~/.kube/config"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

module "release-prometheus-operator" {
  source  = "OpenQAI/release-prometheus-operator/helm"
  version = "0.0.6"

  helm_chart_version     = "8.15.11"
  helm_chart_namespace   = "monitoring"
  skip_crds              =  false
  grafana_image_tag      = "7.0.3"
  grafana_adminPassword  = "ferluko"
  
}

resource "kubernetes_namespace" "student_app_namespace" {
  metadata {
    annotations = {
      name = "student-app"
    }

    labels = {
      app = "student-app"
    }

    name = "student-app"
  }
}


resource "kubernetes_persistent_volume_claim" "mongo_pvc" {
  depends_on = [kubernetes_namespace.student_app_namespace]
  metadata {
    name = "mongo-pvc"
    namespace = "student-app"
  }

  spec {
    access_modes = ["ReadWriteOnce"]

    resources {
      requests = {
        storage = "256Mi"
      }
    }
  }
}

resource "kubernetes_deployment" "mongo_deployment" {
depends_on = [kubernetes_namespace.student_app_namespace]
  metadata {
    name = "mongo"
    namespace = "student-app"
  }

  spec {
    selector {
      match_labels = {
        app = "mongo"
      }
    }

    template {
      metadata {
        labels = {
          app = "mongo"
        }
      }

      spec {
        volume {
          name = "storage"

          persistent_volume_claim {
            claim_name = "mongo-pvc"
          }
        }

        container {
          name  = "mongo"
          image = "mongo:3.6.17-xenial"

          port {
            container_port = 27017
          }

          volume_mount {
            name       = "storage"
            mount_path = "/data/db"
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "mongo_service" {
depends_on = [kubernetes_namespace.student_app_namespace]
  metadata {
    name = "mongo"
    namespace = "student-app"
  }

  spec {
    port {
      port        = 27017
      target_port = "27017"
    }

    selector = {
      app = "mongo"
    }
  }
}

resource "kubernetes_deployment" "student_app_api_deployment" {
depends_on = [kubernetes_namespace.student_app_namespace]
  metadata {
    name = "student-app-api"
    namespace = "student-app"
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "student-app-api"
      }
    }

    template {
      metadata {
        labels = {
          app = "student-app-api"
        }
      }

      spec {
        container {
          name  = "student-app-api"
          image = "flgonzalez/student-app-api:0.0.1-SNAPSHOT"

          port {
            container_port = 8080
          }

          env {
            name  = "MONGO_URL"
            value = "mongodb://mongo:27017/dev"
          }

          image_pull_policy = "Always"
        }
      }
    }
  }
}

resource "kubernetes_service" "student_app_api_service" {
depends_on = [kubernetes_namespace.student_app_namespace]
  metadata {
    name = "student-app-api"
    namespace = "student-app"
  }

  spec {
    port {
      protocol    = "TCP"
      port        = 8080
      target_port = "8080"
    }

    selector = {
      app = "student-app-api"
    }
  }
}

resource "kubernetes_deployment" "student_app_client_deployment" {
depends_on = [kubernetes_namespace.student_app_namespace]
  metadata {
    name = "student-app-client"
    namespace = "student-app"
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "student-app-client"
      }
    }

    template {
      metadata {
        labels = {
          app = "student-app-client"
        }
      }

      spec {
        container {
          name  = "student-app-client"
          image = "flgonzalez/student-app-client"

          port {
            container_port = 80
          }

          image_pull_policy = "Always"
        }
      }
    }
  }
}

resource "kubernetes_service" "student_app_client_service" {
depends_on = [kubernetes_namespace.student_app_namespace]
  metadata {
    name = "student-app-client-service"
    namespace = "student-app"
  }

  spec {
    port {
      protocol    = "TCP"
      port        = 80
      target_port = "80"
    }

    selector = {
      app = "student-app-client"
    }

    type = "ClusterIP"
  }
}


resource "kubernetes_ingress" "student_app_ingress" {
depends_on = [kubernetes_namespace.student_app_namespace]
  metadata {
    name      = "student-app-ingress"
    namespace = "student-app"
    annotations = {
      "nginx.ingress.kubernetes.io/rewrite-target" = "/$1"
    }
  }

  spec {
    rule {
      http {
        path {
          path = "/?(.*)"

          backend {
            service_name = "student-app-client-service"
            service_port = "80"
          }
        }
        path {
          path = "/api/?(.*)"

          backend {
            service_name = "student-app-api"
            service_port = "8080"
          }
        }
      }
    }
  }
}
